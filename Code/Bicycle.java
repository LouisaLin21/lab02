/*Louisa Yuxin Lin 19333472*/ 
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    public Bicycle(String manu, int num, double max){
        //constuctor
        this.manufacturer=manu;
        this.numberGears=num;
        this.maxSpeed=max;
    }

    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears (){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }
    public String toString(){
        return "manufacturer is "+this.manufacturer+ " number of gears is "+this.numberGears+ " Max speed is "+this.maxSpeed;
    }
}
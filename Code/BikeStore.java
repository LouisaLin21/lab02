/*Louisa Yuxin Lin 1933472*/ 
import java.util.*;
public class BikeStore {
    public static void main (String[]args){
        Bicycle[] b = new Bicycle[4];
        b[0]=new Bicycle("b1",23,44.5);
        b[1]=new Bicycle("b2",25,40.5);
        b[2]=new Bicycle("b3",53,47.5);
        b[3]=new Bicycle("b4",13,20.5);
        for(int i=0;i<b.length;i++){
            System.out.println(b[i]);
        }
    }
}
